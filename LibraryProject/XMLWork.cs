﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LibraryProject
{
    class XMLWork
    {
        public void OutputXML(string FileName)
        {
            XDocument xDoc = XDocument.Load(FileName);
            Console.WriteLine(xDoc.ToString());
        }
        public void CreateXML(BooksLibrary Library)
        {
            int count = 0;
            XElement LibraryXElement = new XElement("library");
            LibraryXElement.SetAttributeValue("name", Library.Name);
            LibraryXElement.SetAttributeValue("adress", Library.Adress);
            
            foreach(var D in Library.Departments)
            {
                XElement DepartmentXElement = new XElement("department");
                DepartmentXElement.SetAttributeValue("name", D.Name);
                foreach(var B in Library.Departments[count].Books)
                {
                    XElement BookXElement = new XElement("book");
                    BookXElement.SetAttributeValue("name", B.Name);
                    foreach(var author in B.Authors)
                    {
                        XElement AuthorXElement = new XElement("author");
                       AuthorXElement.SetAttributeValue("authorname", author.Name);
                        AuthorXElement.SetAttributeValue("countbooks", author.CountBooks);
                        BookXElement.Add(AuthorXElement);
                    }
                    
                    BookXElement.SetAttributeValue("countpage", B.CountPage);
                    DepartmentXElement.Add(BookXElement);
                }
                count++;
                LibraryXElement.Add(DepartmentXElement);
            }
            var newXdoc = new XDocument(LibraryXElement);
            newXdoc.Save("Library.xml");
        }

        public void ReadXML(string FileName, ref BooksLibrary Library)
        {
            XDocument xDoc = XDocument.Load(FileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                //Console.ReadKey();
                XElement libraryXel = xDoc.Element("library");

                var libraryName = libraryXel.Attribute("name").Value;//GetAttributeByName("name", libraryXel);
                var libraryAdress = libraryXel.Attribute("adress").Value;//GetAttributeByName("adress", libraryXel);

                Library = new BooksLibrary(libraryName, libraryAdress);

                if (libraryXel != null && libraryXel.HasElements)
                {
                    int i = 0;
                    foreach (var departmentXel in libraryXel.Elements("department"))
                    {
                        var departmentName = departmentXel.Attribute("name").Value;//GetAttributeByName("name", departmentXel);

                        Library.AddDepartment(new Department(departmentName.ToString()));

                        foreach (var bookXel in departmentXel.Elements("book"))
                        {
                            int j = 0;
                            var bookName = bookXel.Attribute("name").Value;
                            var bookCountPage = bookXel.Attribute("countpage").Value;//GetAttributeByName("countpage", bookXel);
                            List<Author> authors = new List<Author>();
                            foreach (var autorsXel in bookXel.Elements("author"))
                            {
                                var authorName = autorsXel.Attribute("authorname").Value;
                                var countBooks = autorsXel.Attribute("countbooks").Value;
                                authors.Add(new Author(authorName, Convert.ToInt32(countBooks)));
                            }
                            Library.Departments[i].AddBook(new Book(bookName, Convert.ToInt32(bookCountPage),authors));
                            //Console.WriteLine(bookName+ bookAuthor+ bookCountPage.ToString());

                        }
                        i++;
                    }
                }
            }
            
        }
        public void EditXML(string FileName)
        {
            XDocument xDoc = XDocument.Load(FileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement libraryXel = xDoc.Element("library");
                if (libraryXel != null && libraryXel.HasElements)
                {
                    XElement departmentXel = libraryXel.Element("department");
                    XElement bookXel = new XElement("book");

                    bookXel.SetAttributeValue("name","new_book");
                    XElement authorsXel = new XElement("author");
                    authorsXel.SetAttributeValue("authorname", "new_authorname");
                    authorsXel.SetAttributeValue("countbooks", "2017");
                    bookXel.SetAttributeValue("countpage", "2017");
                    bookXel.Add(authorsXel);
                    departmentXel.Add(bookXel);
                    xDoc.Save("Library.xml");
                }
            }
        }
        public void RemoveNodeFromXML(string FileName)
        {
            XDocument xDoc = XDocument.Load(FileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement libraryXel = xDoc.Element("library");
                if (libraryXel != null && libraryXel.HasElements)
                {
                    XElement departmentXel = libraryXel.Element("department");
                    if (departmentXel != null)
                    {
                        departmentXel.Remove();
                    }
                }
            }
            xDoc.Save("Library.xml");
        }
    }
}
