﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Xml.Linq;

namespace LibraryProject
{
    class Program
    {

        static void Main(string[] args)
        {
            BooksLibrary MyLibrary = new BooksLibrary("MyLibrary", "adress111");
            List<Author> Authors = new List<Author>()
            { new Author("author1",1), new Author("author2",2), new Author("author3",1), new Author("author4",1) , new Author("author5",1), new Author("author6",1), new Author("author7",1),
                new Author("author8",2) ,new Author("author9",1),new Author("author10",1),new Author("author11",3),new Author("author12",1),new Author("author13",1),new Author("author14",1)};
            List<Book> Books = new List<Book>();
            Books.Add(new Book("book1", 375, new List<Author>() { Authors[0], Authors[1] }));
            Books.Add(new Book("book2", 400, new List<Author>() { Authors[1], Authors[2] }));
            Books.Add(new Book("book3", 450, new List<Author>() { Authors[3] }));
            Books.Add(new Book("book4", 200, new List<Author>() { Authors[4] }));
            Books.Add(new Book("book5", 100, new List<Author>() { Authors[5], Authors[6], Authors[7] }));
            Books.Add(new Book("book6", 820, new List<Author>() { Authors[7] }));
            Books.Add(new Book("book7", 450, new List<Author>() { Authors[8]}));
            Books.Add(new Book("book8", 283, new List<Author>() { Authors[9], Authors[10] }));
            Books.Add(new Book("book9", 120, new List<Author>() { Authors[10] }));
            Books.Add(new Book("book10", 453, new List<Author>() { Authors[10] }));
            Books.Add(new Book("book11", 120, new List<Author>() { Authors[11] }));
            Books.Add(new Book("book12", 460, new List<Author>() { Authors[12], Authors[12] }));
            

            MyLibrary.AddDepartment(new Department("IT"));
            MyLibrary.Departments[0].AddBook(Books[0]);
            MyLibrary.Departments[0].AddBook(Books[1]);
            MyLibrary.Departments[0].AddBook(Books[2]);
            MyLibrary.Departments[0].AddBook(Books[3]);

            MyLibrary.AddDepartment(new Department("WorldLiterature"));
            MyLibrary.Departments[1].AddBook(Books[4]);
            MyLibrary.Departments[1].AddBook(Books[5]);
            MyLibrary.Departments[1].AddBook(Books[6]);

            MyLibrary.AddDepartment(new Department("UkrainianLiterature"));
            MyLibrary.Departments[2].AddBook(Books[7]);
            MyLibrary.Departments[2].AddBook(Books[8]);
            MyLibrary.Departments[2].AddBook(Books[9]);
            MyLibrary.Departments[2].AddBook(Books[10]);
            MyLibrary.Departments[2].AddBook(Books[11]);

            

            Authors.Sort();
            Authors.Reverse();

            MyLibrary.Departments.Sort();
            foreach(var i in Authors)
            {
                Console.WriteLine(i);
            }

            foreach(var D in MyLibrary.Departments)
            {
                D.Books.Sort();
            }
            
            Console.WriteLine();
            MyLibrary.Output();

            Console.WriteLine();
            Console.WriteLine("Author with maximum count of books - {0}",Authors.Max());
            Console.WriteLine();
            Console.WriteLine("Department with maximum count of books - {0}",MyLibrary.Departments.Max());
            Console.WriteLine();

            var result = MyLibrary.Departments.Where(d => d.CountOfBooks() > 3);
            var result2 = Authors.OrderBy(a => a.CountOfBooks());

            Console.WriteLine("Departments where count of books more than 3:");
            foreach (var MD in result)
            {
                Console.WriteLine(MD.ToString());
            }
            Console.WriteLine("Authors sorted by count of books");
            foreach (var A in result2)
            {
                Console.WriteLine(A.ToString());
            }
            Book book = MyLibrary.Departments.Min(b => b.Books.Min());
            
            Console.WriteLine("Book with minimum count of pages in library - {0}",book);

            ICountingBooks MyLibrary2 = MyLibrary;
            Console.WriteLine("Count of books in library - {0}",MyLibrary2.CountOfBooks());
            //------------------------------------------------------Json----------------------------------------------------------//
            JSONSerializer J = new JSONSerializer();
            J.Serialize(MyLibrary);

            BooksLibrary NewLibrary = J.Deserialize("library.json");
            NewLibrary.Output();
            //------------------------------------------------------Json----------------------------------------------------------//
            //-------------------------------------------------------XML----------------------------------------------------------//
            string xmlName = "Library.xml";

            XMLWork Xml = new XMLWork();

            Console.WriteLine("Creating XML-Document:");
            Xml.CreateXML(MyLibrary);
            Xml.OutputXML(xmlName);

            Console.WriteLine("Adding new book for first department:");
            Xml.EditXML(xmlName);
            Xml.OutputXML(xmlName);

            BooksLibrary MyLibrary3 = null;
            List<Author> Author2 = new List<Author>();
            Console.WriteLine("Reading XML-Document:");
            Xml.ReadXML(xmlName, ref MyLibrary3);
            MyLibrary3.Output();

            Console.WriteLine("Removing first department:");
            Xml.RemoveNodeFromXML(xmlName);
            Xml.OutputXML(xmlName);
            //---------------------------------------------------------XML--------------------------------------------------------//

            Console.ReadKey();
        }
    }
}
