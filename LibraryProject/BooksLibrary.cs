﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace LibraryProject
{
    [DataContract]
    class BooksLibrary : BaseEntity, ICountingBooks
    {
        [DataMember]
        public string Adress { get; set; }
        [DataMember]
        public List<Department> Departments { get; set; }
        
        public BooksLibrary() : base()
        {
            this.Adress = String.Empty;
            Departments = null;
        }
        public BooksLibrary(string name, string adress) : base(name)
        {
            this.Adress = adress;
            this.Departments = new List<Department>();
        }
        public void AddDepartment(Department department)
        {
            Departments.Add(department);
        }
        public int CountOfBooks()
        {
            int SumBooks = 0;
            foreach (var D in Departments)
            {
                SumBooks += D.CountOfBooks();
            }
            return SumBooks;
        }

        public override string GetInformation()
        {
            var info = new StringBuilder();
            info.AppendFormat("Name - {0}, ", Name);
            info.AppendFormat("Adress - {0}.", Adress);
            info.AppendFormat("CountOfDepartments - {0}", Departments.Count);
            return info.ToString();
        }
        public override string ToString()
        {
            return GetInformation();
        }
        public void Output()
        {
            Console.WriteLine(this.ToString());
            foreach (var MD in Departments)
            {
                Console.WriteLine(MD.ToString());
                foreach (var B in MD.Books)
                {
                    Console.WriteLine(B.ToString());
                }
            }
        }
    }
}
