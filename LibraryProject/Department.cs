﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace LibraryProject
{
    [DataContract]
    public class Department:BaseEntity,IComparable<Department>,ICountingBooks
    {
        [DataMember]
        public List<Book> Books { get; set; }
        public Department():base()
        {
            this.Books = null;
        }
        public Department(string name) : base(name)
        {
            this.Books = new List<Book>();
        }
        public void AddBook(Book book)
        {
            //bool f = true;
            Books.Add(book);
            //if(author.Count==0 )
            //{
            //    author.AddRange(book.Authors);
            //    for (int i = 0; i < author.Count; i++)
            //    {
            //        author[i].AddBook(book);
            //    }
            //}
            //else
            //{
            //    for(int i=0;i<author.Count;i++)
            //    {
            //        for (int j = 0; j < book.Authors.Count; j++)
            //        {
            //            if (book.Authors[j].Name == author[i].Name)
            //            {
            //                f = true;
            //                author[i].AddBook(book);
            //                break;
            //            }
            //            else
            //            {
            //                f = false;
            //            }
            //        }
            //    }
            //    if(f==false)
            //    {
            //        author.AddRange(book.Authors);
            //        for (int i = author.Count - 1; i > author.Count-book.Authors.Count; i--)
            //        {
            //            author[i].AddBook(book);
            //        }
            //    }
            //}
        }
        public int CountOfBooks()
        {
            return Books.Count;
        }

        public int CompareTo(Department obj)
        {
            if (obj != null)
            {
                if (this.Books.Count > obj.Books.Count)
                {
                    return 1;
                }
                if (this.Books.Count < obj.Books.Count)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                throw new ArgumentException("Parameter cannot be null");
            }
        }
        public override string GetInformation()
        {
            var info = new StringBuilder();
            info.AppendFormat("Name - {0}, ", Name);
            info.AppendFormat("CountOfBooks - {0}.", Books.Count);
            return info.ToString();
        }

        public override string ToString()
        {
            return GetInformation();
        }

    }
    
}
