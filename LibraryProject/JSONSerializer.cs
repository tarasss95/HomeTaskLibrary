﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;


namespace LibraryProject
{
    class JSONSerializer
    {
        public void Serialize(BooksLibrary Library)
        {
            FileStream fstream = new FileStream("library.json", FileMode.OpenOrCreate);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(BooksLibrary));
            ser.WriteObject(fstream, Library);
            fstream.Position = 0;
            StreamReader sr = new StreamReader(fstream);
            Console.Write("JSON form of BooksLibrary object: ");
            Console.WriteLine(sr.ReadToEnd());
            fstream.Close();
            sr.Close();
            
        }
        public BooksLibrary Deserialize(string FileName)
        {
            FileStream fstream = File.OpenRead(FileName);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(BooksLibrary));
            return (BooksLibrary)ser.ReadObject(fstream);
        }
    }
}
