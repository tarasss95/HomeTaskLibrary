﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    public class Author : BaseEntity, IComparable<Author>, ICountingBooks
    {
        public int CountBooks { get; set; }

        public Author() : base()
        {
            this.CountBooks=0 ;
        }
        public Author(string name,int countbooks) : base(name)
        {
            this.CountBooks=countbooks;
        }
        public int CompareTo(Author obj)
        {
            if (obj != null)
            {
                if (this.CountBooks > obj.CountBooks)
                {
                    return 1;
                }
                if (this.CountBooks < obj.CountBooks)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                throw new ArgumentException("Parameter cannot be null");
            }
        }
        public int CountOfBooks()
        {
            return CountBooks;
        }
        public override string GetInformation()
        {
            var info = new StringBuilder();
            info.AppendFormat("Name - {0}, ", Name);
            info.AppendFormat("CountOfBooks - {0}.", CountBooks);
            return info.ToString();
        }

        public override string ToString()
        {
            return GetInformation();
        }
    }
}
