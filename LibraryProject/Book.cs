﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace LibraryProject
{
    [DataContract]
    public class Book : BaseEntity, IComparable<Book>
    {
        [DataMember]
        public List<Author> Authors { get; set; }
        [DataMember]
        public int CountPage { get; private set; }
        public Book() : base()
        {
            CountPage = 0;
            Authors = new List<Author>();
        }
        public Book(string name, int countpage, List<Author> author) : base(name)
        {
            this.CountPage = countpage;
            this.Authors = author;

        }
        public int CompareTo(Book obj)
        {
            if (obj != null)
            {
                if (this.CountPage > obj.CountPage)
                {
                    return 1;
                }
                if (this.CountPage < obj.CountPage)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                throw new ArgumentException("Parameter cannot be null");
            }
        }
        public override string GetInformation()
        {
            var info = new StringBuilder();
            info.AppendFormat("Name - {0}, ", Name);
            info.AppendFormat("CountPage - {0}, ", CountPage);
            foreach(var N in Authors)
            {
                info.AppendFormat("AuthorName - {0}, ", N.Name);
            }
            return info.ToString();
        }

        public override string ToString()
        {
            return GetInformation();
        }

    }
}
