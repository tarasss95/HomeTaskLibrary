﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace LibraryProject
{
    [DataContract]
    public abstract class BaseEntity
    {
        [DataMember]
        public string Name { get; set; }
        protected BaseEntity()
        {
            this.Name = String.Empty;
            //this.Count = 0;
        }
        protected BaseEntity(string name)
        {
            this.Name = name;
            //this.Count = count1;
        }
        //public abstract string GetEntityType();
        public virtual string GetInformation()
        {
            return "Name - " + Name + ".";
        }
        public override string ToString()
        {
            return GetInformation();
        }


    }
}
